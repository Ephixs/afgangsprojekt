class Result < ActiveRecord::Base
  serialize :data, Array
  serialize :hashed_data, Array
  belongs_to :website

end
