class Website < ActiveRecord::Base
  serialize :results, Hash
  serialize :data, Array
  has_one :result
  has_one :event_wrapper
  has_many :events

  after_create :create_result_and_event_wrapper

  def types
    ["Event side","Event kalender med undersider","Event kalender med alt info"]
  end

  def create_result_and_event_wrapper
    self.result = Result.new(:website_id => self.id, :data => [])
    self.event_wrapper = EventWrapper.new(:website_id => self.id)
  end

end
