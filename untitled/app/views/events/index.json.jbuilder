json.array!(@events) do |event|
  json.extract! event, :id, :title, :happens_at, :description, :price, :venue_name
  json.url event_url(event, format: :json)
end
