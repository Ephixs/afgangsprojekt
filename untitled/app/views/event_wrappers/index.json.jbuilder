json.array!(@event_wrappers) do |event_wrapper|
  json.extract! event_wrapper, :id, :identifier
  json.url event_wrapper_url(event_wrapper, format: :json)
end
