require 'uri'

class EventWrappersController < ApplicationController
  before_action :set_event_wrapper, only: [:show, :edit, :update, :destroy]
  include ScraperHelper, UrlHelper

  # GET /event_wrappers
  # GET /event_wrappers.json
  def index
    @event_wrappers = EventWrapper.all
  end

  # GET /event_wrappers/1
  # GET /event_wrappers/1.json
  def show
  end

  # GET /event_wrappers/new
  def new
    @event_wrapper = EventWrapper.new
  end

  # GET /event_wrappers/1/edit
  def edit
  end

  # POST /event_wrappers
  # POST /event_wrappers.json
  def create
    @event_wrapper = EventWrapper.new(event_wrapper_params)

    respond_to do |format|
      if @event_wrapper.save
        format.html { redirect_to @event_wrapper, notice: 'Event wrapper was successfully created.' }
        format.json { render :show, status: :created, location: @event_wrapper }
      else
        format.html { render :new }
        format.json { render json: @event_wrapper.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /event_wrappers/1
  # PATCH/PUT /event_wrappers/1.json
  def update
    params[:identifier] = @event_wrapper.identifier
    if params[:href_css] != nil
      params[:href_css] = params[:href_css]
    end
    respond_to do |format|
      if @event_wrapper.update(event_wrapper_params)
        if @event_wrapper.href_css != nil
        @result = @event_wrapper.website.result
        nodes = get_all_nodes_with_css_class(get_html(@event_wrapper.website.url),@event_wrapper.identifier)
        links = get_all_nodes_with_css_class(nodes.first,@event_wrapper.href_css).first.attribute("href").text
        if !is_url(links)
          uri = URI.parse(@result.website.url)
          links = uri.scheme + "://" + uri.host + links
          else
          links = links
        end
          @result.update_attributes(:link_url => links)
          format.html {redirect_to edit_result_path(@result)}
        else
        format.html { redirect_to @event_wrapper, notice: "Event wrapper was successfully updated." }
        format.json { render :show, status: :ok, location: @event_wrapper }
        end
      else
        format.html { render :edit }
        format.json { render json: @event_wrapper.errors, status: :unprocessable_entity }
        end
    end
  end

  # DELETE /event_wrappers/1
  # DELETE /event_wrappers/1.json
  def destroy
    @event_wrapper.destroy
    respond_to do |format|
      format.html { redirect_to event_wrappers_url, notice: 'Event wrapper was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_event_wrapper
      @event_wrapper = EventWrapper.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def event_wrapper_params
      params.permit(:identifier, :website_id, :href_css)
    end
end
