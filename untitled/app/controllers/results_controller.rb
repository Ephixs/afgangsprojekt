class ResultsController < ApplicationController
  include DateHelper, ScraperHelper, ListEventHelper
  helper_method :get_array_with_hashs_from_to_arrays_of_same_size
  before_action :set_result, only: [:show, :edit, :update, :destroy]

  # GET /Results
  # GET /Results.json
  def index
    @results = Result.all
  end

  # GET /Results/1
  # GET /Results/1.json
  def show
  end

  # GET /Results/new
  def new
    @result = Result.new
  end

  # GET /Results/1/edit
  def edit
    @result = Result.find(params[:id])
  end

  # POST /Results
  # POST /Results.json
  def create
    @result = Result.new(result_params)

    respond_to do |format|
      if @result.save
        format.html { redirect_to @result, notice: 'Result was successfully created.' }
        format.json { render :show, status: :created, location: result }
      else
        format.html { render :new }
        format.json { render json: @result.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /Results/1
  # PATCH/PUT /Results/1.json
  def update

    respond_to do |format|
      if @result.update(result_params )
        if @result.hashed_data.size < @result.data.size
          format.html {redirect_to edit_result_path(@result)}
        else
          if @result.website.page_type == @result.website.types[1]
            scrapeEvents(@result.website.id)
            format.html { redirect_to websites_path}
          else
            create_event(format)
          end
          # if @event != nil
          #   format.html { redirect_to event_path(@event), notice: 'Event was created' }
          # else
          #   format.html { redirect_to @result, notice: 'Result was successfully updated.' }
          #   format.json { render :show, status: :ok, location: @result }
          # end
        end
      else
        format.html { render :edit }
        format.json { render json: @result.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /Results/1
  # DELETE /Results/1.json
  def destroy
    @result.destroy
    respond_to do |format|
      format.html { redirect_to results_url, notice: 'Result was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def get_array_with_hashs_from_to_arrays_of_same_size(a1, a2)
    array = Array.new
    if a1.size == a2.size
      i = 0
      while i < a1.size
        hash = Hash[a1[i] => a2[i]]
        array[array.size] = hash
        i = i +1
      end
    end
    return array
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_result
      @result = Result.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def result_params
      params.permit(:time_format,{:hashed_data => []},{:data => []})
    end

    def create_event(format)
      if @result.link_url == nil
      url = @result.website.url
      else
        url = @result.link_url
        end
      title = ""
      date = ""
      venue_name = ""
      price = ""
      description = ""
      keys = @result.hashed_data
      values = @result.data
      array_with_hashes = get_array_with_hashs_from_to_arrays_of_same_size(keys, values)
      array_with_hashes.each do |d|
        d = d.to_a
        case
          when d[0][0] == "titel"
            title = title + get_node_from_xpath(url,d[0][1]).text
          when d[0][0] == "tidspunkt"
            date = date + get_node_from_xpath(url,d[0][1]).text
          when d[0][0] == "beskrivelse"
            description = "#{description}#{get_node_from_xpath(url,d[0][1]).text} "
          when d[0][0] == "pris"
            price = price + get_node_from_xpath(url,d[0][1]).text
          when d[0][0] == "sted"
            venue_name = venue_name + get_node_from_xpath(url,d[0][1]).text
        end
      end
      date = get_date_from_string(date,@result.time_format)
      price = get_numbers(price).first
      @event = Event.new(:title => title, :website_id => @result.website_id.to_i , :happens_at => date, :venue_name => venue_name, :price => price, :description => description)
      if @event.save
        format.html { redirect_to event_path(@event), notice: 'Event was created' }
      end
    end
end
