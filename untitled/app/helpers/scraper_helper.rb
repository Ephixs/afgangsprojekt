require 'nokogiri'
require 'open-uri'


module ScraperHelper
#Returns the full html document
  def get_html(url)
    Nokogiri::HTML(open("#{url}"))
  end

#Returnes a hashed defination of the result it gets by searching on a word
  def compare_word_to_css(word, html)
    result = html.css(".#{word}").collect {|node| node.text.strip}
    hash = {"#{word}" => result}
  end

  def get_node_from_css(word, html)
    html.css("#{word}")
  end

  def compare_word_to_html(word, html)
    result = html.xpath("//#{word}").map(&:attributes)
    hash = {"#{word}" => result}
  end


  def start_search(url)
    word_list = ["p", "a", "div", "img", "span", "href", "class", "title", "h1", "h2", "h3", "r", "u", "td", "sup", "q"]
    result_list = Hash.new
    html = get_html(url)
    word_list.each do |i|
      result_list.merge!(compare_word_to_html(i, html))
    end
    result_list
  end

#Gets all text between all tags from HTML
  def get_all_text(html)
    html.xpath("//text()").to_s
  end

  def get_all_elements(url)
    html = get_html(url)
    html.xpath("//text()")
  end

  def get_xpath_from_word(html, word)
    node = get_node_from_css(word, html)
    if node.class == Nokogiri::XML::NodeSet
      node = node.pop
    end
    path = Nokogiri::CSS.xpath_for node.css_path
    p path
    html.xpath(path.first).first.text
  end

  def get_xpath_from_node(node)
    if node.class == Nokogiri::XML::NodeSet
      node = node.pop
    end
    Nokogiri::CSS.xpath_for node.css_path
  end

  def get_node_from_xpath(url, xpath)
    get_html(url).xpath("#{xpath}")
  end


  def remove_all_none_tag_nodes(node_array)
    nodes = Array.new
    node_array.each do |n|
      if n != nil
        if !n.text.include?("\n")
          nodes.insert(nodes.size, n)
        end
      end

    end
    return nodes
  end

  def remove_last_part_of_path(path)
    path = path.split("/")
    path.pop
    path = path.join("/")
  end

  def get_css_class(node)
    node.attribute('class').value
  end

  def get_all_nodes_with_css_class(html, css)
    html.xpath("//*[@class=\"#{css}\"]")
  end

  def get_all_nodes_with_css_id(html, id)
    html.xpath("//*[@id=\"#{id}\"]")
  end

  def get_all_links(nodeset)
    get_node_from_css("a",nodeset)
  end

end