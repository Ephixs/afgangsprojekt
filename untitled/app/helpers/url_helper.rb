module UrlHelper

  def is_url(url)
    url.include?('.')
  end

end
