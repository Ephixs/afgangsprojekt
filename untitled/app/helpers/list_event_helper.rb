require 'uri'

module ListEventHelper
  include ScraperHelper, UrlHelper

  def scrapeEvents(website_id)
    website = Website.find(website_id)
    if website.page_type == website.types[1]
      event_wrap = EventWrapper.find(website.event_wrapper.id)
      result = Result.find(website.result.id)
      nodes = get_all_nodes_with_css_class(get_html(website.url),event_wrap.identifier)
      link_nodes = get_all_nodes_with_css_class(nodes,event_wrap.href_css)

      link_nodes.each do |l|
        link = l.attribute("href")
        if !is_url(link)
          uri = URI.parse(@result.website.url)
          link = uri.scheme + "://" + uri.host + link
        else
          link = link
        end

        title = ""
        date = ""
        venue_name = ""
        price = ""
        description = ""
        keys = result.hashed_data
        values = result.data
        array_with_hashes = get_array_with_hashs_from_to_arrays_of_same_size(keys, values)
        array_with_hashes.each do |d|
          d = d.to_a
          case
            when d[0][0] == "titel"
              title = title + get_node_from_xpath(link,d[0][1]).text
            when d[0][0] == "tidspunkt"
              date = date + get_node_from_xpath(link,d[0][1]).text
            when d[0][0] == "beskrivelse"
              description = "#{description}#{get_node_from_xpath(link,d[0][1]).text} "
            when d[0][0] == "pris"
              price = price + get_node_from_xpath(link,d[0][1]).text
            when d[0][0] == "sted"
              venue_name = venue_name + get_node_from_xpath(link,d[0][1]).text
          end
        end
        date = get_date_from_string(date,@result.time_format)
        price = get_numbers(price).first
        @event = Event.new(:title => title, :website_id => @result.website_id.to_i , :happens_at => date, :venue_name => venue_name, :price => price, :description => description)
        @event.save
      end

      end

    end


end