module DateHelper

  def date_formats
    return ["d(05) - m(12) - y(2015) - t(20:00)", "m(12) - d(05) - y(2015) - t(20:00)", "d(05) - m(juni) - y(2015) - t(20:00)", "d(05) - m(juni) - y(2015) - t(20:00)"]
  end

  def get_date_from_string(string, date_format)
    date = nil
    numbers = get_numbers(string)
    month = get_month_if_string(string)
    if month != nil
      month = month[0..2]
      month = check_for_danish_short_month_name(month)
      month = month.capitalize
    end
    case
      when date_formats[0] == date_format
        if numbers.size >= 4 || numbers.size <= 5
          if numbers.size == 4
            date = "#{numbers[0]}/#{numbers[1]}/#{numbers[2]} #{numbers[3]}"
          else
            date = "#{numbers[0]}/#{numbers[1]}/#{numbers[2]} #{numbers[3]}:#{numbers[4]}"
          end
          date = Time.strptime(date, '%d/%m/%Y %H:%M')
        else
          #Dato passede ikke, der er sket en fejl
        end
      when date_formats[1] == date_format
        if numbers.size >= 4 || numbers.size <= 5
          if numbers.size == 4
            date = "#{numbers[0]}/#{numbers[1]}/#{numbers[2]} #{numbers[3]}"
          else
            date = "#{numbers[0]}/#{numbers[1]}/#{numbers[2]} #{numbers[3]}:#{numbers[4]}"
          end
          date = Time.strptime(date, '%m/%d/%Y %H:%M')
        else
          #Dato passede ikke, der er sket en fejl
        end
      when date_formats[2] == date_format
        if month != nil
          if numbers.size >= 3 || numbers.size <= 4
            if numbers.size == 3
              date = "#{numbers[0]}/#{month}/#{numbers[1]} #{numbers[2]}"
            else
              date = "#{numbers[0]}/#{month}/#{numbers[1]} #{numbers[2]}:#{numbers[3]}"
            end
            date = Time.strptime(date, '%d/%b/%Y %H:%M')
          end
        end
      when date_formats[3] == date_format
        if month != nil
          if numbers.size >= 3 || numbers <= 4
            if numbers.size == 3
              date = "#{numbers[0]}/#{month}/#{numbers[1]} #{numbers[2]}"
            else
              date = "#{numbers[0]}/#{month}/#{numbers[1]} #{numbers[2]}:#{numbers[3]}"
            end
            p date
            date = DateTime.strptime(date, '%d/%B/%Y %H:%M')
            DateTime.strptime(date, '%d/%B/%Y %H:%M')
          end
        end
    end

  end

  def check_for_danish_short_month_name(month)
    if month == "maj"
      month = "may"
    elsif month == "okt"
      month = "oct"
    end
    return month
  end

  def date_month_year_time_and_all_is_numbers(date)

  end

  def date_month_year_time_and_month_is_string(date)

  end

  def month_date_year_time_and_all_is_numbers(date)

  end

  def month_date_year_time_and_month_is_string(date)

  end

  def get_numbers(string)
    strings = Array.new
    if string.include?(":")
      string = string.split(":")
      string = string.join(" ")
    end
    if string.include?("/")
      string = string.split("/")
      string = string.join(" ")
    end
    string = string.split(" ")
    string.each do |s|
      if s =~ /\d/
        s = s.scan(/\d/).join('')
        strings.insert(strings.size, s)
      end
    end
    return strings
  end

  def get_month_if_string(string)
    date = nil
    get_all_month_arrays.each do |a|
      if date == nil
        date = check_for_words_in_string(a,string)
      end
    end
    return date
  end

  def check_for_words_in_string(word_array, string)
    matched_word = nil
    string = string.downcase
    word_array.each do |d|
      if string.include? d.downcase
        matched_word = d
      end
    end
    return matched_word
  end

  def get_all_month_arrays
    [get_long_month_names_danish, get_short_month_names_danish, get_long_month_names_english, get_short_month_names_english]
  end

  def get_long_month_names_danish
    ["januar","februar","marts","april", "maj","juni","juli","august","september","oktober","november","december"]
  end

  def get_short_month_names_danish
    ["jan","feb","mar","apr", "maj","jun","jul","aug","sep","okt","nov","dec"]
  end

  def get_long_month_names_english
    ["January","February","March","April","May","June","July","August","September","October","November","December"]
  end

  def get_short_month_names_english
    ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]
  end


end