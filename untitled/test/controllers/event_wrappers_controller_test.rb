require 'test_helper'

class EventWrappersControllerTest < ActionController::TestCase
  setup do
    @event_wrapper = event_wrappers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:event_wrappers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create event_wrapper" do
    assert_difference('EventWrapper.count') do
      post :create, event_wrapper: { identifier: @event_wrapper.identifier }
    end

    assert_redirected_to event_wrapper_path(assigns(:event_wrapper))
  end

  test "should show event_wrapper" do
    get :show, id: @event_wrapper
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @event_wrapper
    assert_response :success
  end

  test "should update event_wrapper" do
    patch :update, id: @event_wrapper, event_wrapper: { identifier: @event_wrapper.identifier }
    assert_redirected_to event_wrapper_path(assigns(:event_wrapper))
  end

  test "should destroy event_wrapper" do
    assert_difference('EventWrapper.count', -1) do
      delete :destroy, id: @event_wrapper
    end

    assert_redirected_to event_wrappers_path
  end
end
