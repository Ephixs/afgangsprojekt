class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :title
      t.datetime :happens_at
      t.text :description
      t.integer :price
      t.string :venue_name
      t.integer :website_id
      t.timestamps null: false
    end
  end
end
