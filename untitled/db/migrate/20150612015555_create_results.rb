class CreateResults < ActiveRecord::Migration
  def change
    create_table :results do |t|
      t.integer :website_id
      t.text :data
      t.text :hashed_data
      t.string :time_format
      t.string :link_url
      t.timestamps null: false
    end
  end
end
