class CreateWebsites < ActiveRecord::Migration
  def change
    create_table :websites do |t|
      t.string :name
      t.string :url
      t.string :page_type
      t.text :results, :limit => 999999999
      t.text :data
      t.timestamps null: false
    end
  end
end
