class CreateEventWrappers < ActiveRecord::Migration
  def change
    create_table :event_wrappers do |t|
      t.string :identifier
      t.integer :website_id
      t.string :href_css
      t.timestamps null: false
    end
  end
end
