# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150615221023) do

  create_table "event_wrappers", force: :cascade do |t|
    t.string   "identifier", limit: 255
    t.integer  "website_id", limit: 4
    t.string   "href_css",   limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "events", force: :cascade do |t|
    t.string   "title",       limit: 255
    t.datetime "happens_at"
    t.text     "description", limit: 65535
    t.integer  "price",       limit: 4
    t.string   "venue_name",  limit: 255
    t.integer  "website_id",  limit: 4
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "results", force: :cascade do |t|
    t.integer  "website_id",  limit: 4
    t.text     "data",        limit: 65535
    t.text     "hashed_data", limit: 65535
    t.string   "time_format", limit: 255
    t.string   "link_url",    limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "websites", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "url",        limit: 255
    t.string   "page_type",  limit: 255
    t.text     "results",    limit: 4294967295
    t.text     "data",       limit: 65535
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

end
