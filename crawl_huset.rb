require 'wombat'

def crawl_huset(start_date)
  event = Wombat.crawl do
    base_url = 'http://huset.dk'
    base_url base_url
    path '/program/'

    events 'css=.event_wrapper', :iterator do
      title css: '.intro .title'
      description css: '.content_right .description'
      date css: '.date' do |d|
      d[4..d.length]
    end
    price css: '.content_left' do |p|
    if p.include? "kr."
      p[(p.index('kr.') - 4)..(p.index('kr.') - 2)]
    else
      p = 0
    end
  end
  venue_name 'Huset'
  time css: '.time' do |t|
  t.delete "Kl. "
end
image 'css=.image', :html do |e|
  base_url + e.gsub(/" width.*/, '').gsub("<img src=\"", '/') if e != ''
end
end
end
event['events'] = event['events'].each { |e| e.store 'happens_at', Time.strptime("#{e['date']} #{e['time']}", '%d.%m.%y %H:%M') }
end

crawl_huset(Time.now)