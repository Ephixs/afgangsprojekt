require 'wombat'

def crawl_musikkenshus(start_date)
  event = Wombat.crawl do
    base_url = 'http://www.musikkenshus.dk'

    base_url base_url
    path '/'

    events 'xpath=//div[@class="event-teaser-figure"][1]//a', :follow do
      title css: '.event-content__caption'
      day css: '.date__month'
      month css: '.date__day'
      year css: '.date--year'
      venue_name 'Musikkenshus'
      time css: '.event__details__time' do |t|
      t.delete "Kl. "
    end
    price css: '.event__details__admission' do |p|
    p = p.scan(/\d+/)
    t = 0
    p.each do |i|
      if i.length >= 2
        t = i
      end
    end
    p = t
    if p == nil
      p = 0
    end
  end
  description css: '.event-content'
  image_url 'xpath=//div[@class="page-top__content"]/img[@class="rsImg js-responsive-image"]/@data-small' do |i|
    base_url + i.gsub(/\?.*/, '')
  end
end
end
event['events'] = event['events'].each { |e| e.store 'happens_at', Time.strptime("#{e['day']}#{e['month']}/#{e['year']} #{e['time']}", '%d/%m/%Y %H:%M') }
end

crawl_musikkenshus(Time.now)