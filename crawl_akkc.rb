require 'wombat'

def crawl_akkc(start_date)
  event = Wombat.crawl do
    base_url = 'http://www.akkc.dk'
    base_url base_url
    path '/musik--teater?when=3m&what=1116&where=1143'

    times 'xpath=//table[@id="eventcalendar"]/tbody/tr', :iterator do
      time css: '.info' do |f|
      f = f.gsub("Kongres & Kultur Center", "").gsub("Aalborghus Slots ", "").gsub("Hus", "").gsub(/\u00a0/, "").gsub(/\s+/m, ' ').strip.split(" ")
    end
      title 'xpath=./td/@title'

      details 'xpath=./td/div[@class="actions"]/a[@class="readbutton"]', :follow do
      title 'xpath=//div[@id="pagetitlewrapper"]/h1'
      venue_name css: '.eventplace'
      image_url1 'xpath=//p[@class="right"]/img/@src' do |i|
        if i != nil
          base_url + i
        end
      end
      image_url2 'xpath=//span[@class="intro"]/img/@src' do |p|
        if p != nil
          base_url + p
        end
      end
      image_url3 'xpath=//img[@class="right"]/@src' do |p|
        if p != nil
          base_url + p
        end
      end
      description 'xpath=//div[@class="normalcol"]'
      year 'xpath=//span[@class="monthname"]' do |y|
        y.split(" ").last
      end
      price css: '.eventbuyarea' do |p|
        p.scan(/\d+/).first
      end
    end
  end
end

ev_list = event['times']
event_list = Array.new
ev_list.each do |i|
  i['time'] = make_time_array(i['time'])
  i['time'] = remove_empty_from_times(i['time'])
  i['time'] = array_to_hash(i['time'])

  if i['details'][0]['image_url1'].class != NilClass
    image_url = i['details'][0]['image_url1']
  elsif i['details'][0]['image_url2'].class != NilClass
    image_url = i['details'][0]['image_url2']
  else
    image_url = i['details'][0]['image_url3']
  end
  img_hash = {'image_url' => image_url}

  hash = {'happens_at' => Time.strptime("#{i['time']['date']}/#{i['time']['month'][0..2].gsub(/maj|okt/, 'maj' => 'May', 'okt' => 'Oct')}/#{i['details'][0]['year']} #{i['time']['time']}", '%d/%b/%Y %H:%M')}
  tag_hash = {'tag_list' => [i['time']['tag']]}
  title_hash = {'title' => i['title']}
  venue_hash = {'venue_name' => i['details'][0]['venue_name']}
  description_hash = {'description' => i['details'][0]['description'].gsub("$('#fbComments').attr('data-href', window.location.href);", "")}
  price_hash = {'price' => i['details'][0]['price']}
  detail = Hash.new
  detail = detail.merge(title_hash)
  detail = detail.merge(venue_hash)
  detail = detail.merge(description_hash)
  detail = detail.merge(price_hash)
  detail = detail.merge(img_hash)
  detail = detail.merge(tag_hash)
  detail = detail.merge(hash)
  event_list.insert(event_list.size-1, detail)
end
return event_list
end

def make_time_array(array)
  array.map! do |e|
    if e.include?(':')
      z = e.index(':') + 2
      e = [e[0..z], e[(z + 1)..-1]]
    else
      e = e.gsub("kl.", "").gsub(".","").gsub(",", "").gsub("-", "")
    end
  end
  return array.flat_map {|e| e}
end

def remove_empty_from_times(array)
    (0..array.size-1).each do |i|
      if array[i].to_s.length == 0
        array.delete_at(i)
    end
  end
  return array
end

def array_to_hash(array)
  hash_names = Array.new
  (0..array.size-1).each do |t|
    hash_names.insert(hash_names.size, get_times_name(t))
  end
  return Hash[hash_names.zip(array)]
end


def get_times_name(index)
  case index
    when 0
      return "0"
    when 1
      return "date"
    when 2
      return "month"
    when 3
      return "time"
    when 4..10
      return "tag"
  end
end

pp crawl_akkc(Date.today)
pp Time.now